package Models;

import java.util.List;

public class Zone {

    private String id;
    private List<Point> vertices;
    private Point center;
    private double radius;
    private boolean polygonBased;

    public Zone(String id, List<Point> edges, Point center, double radius, boolean polygonBased) {
        this.id = id;
        this.vertices = edges;
        this.center = center;
        this.radius = radius;
        this.polygonBased = polygonBased;
    }

    public List<Point> getVertices() {
        return vertices;
    }

    public Point getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public String getId(){
        return this.id;
    }

    public boolean getPolygonBased() {
        return this.polygonBased;
    }
}
