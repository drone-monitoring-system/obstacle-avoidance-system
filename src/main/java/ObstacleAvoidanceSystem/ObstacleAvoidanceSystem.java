package ObstacleAvoidanceSystem;

import Models.*;
import Models.DBModels.DBDrone;
import Models.DBModels.DBZone;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;
import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;
import com.twilio.rest.api.v2010.account.Message;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/*
Created by : Jonathan Cournoyer 40031095
Subject : COEN/ELEC 490

 */

public class ObstacleAvoidanceSystem {

    public static final String ACCOUNT_SID = ""; // TWILIO ACCOUNT_SID
    public static final String AUTH_TOKEN = ""; // TWILIO AUTH_TOKEN

    public static final String TWILIO_NUMBER_FLORIDA = ""; // TWILIO NUMBER FROM FLORIDA WE HAD
    public static final String TWILIO_NUMBER_MONTREAL = ""; // TWILIO NUMBER FROM MONTREAL WE HAD

    public static final String droneTargetPhone = ""; // Where the texts are sent to for detection

    private HashMap<String, Zone> zones;
    private HashMap<String, Drone> drones;

    private HashMap<String, String> isBeingControlled;

    public ObstacleAvoidanceSystem() {
        this.isBeingControlled = new HashMap<>();
        this.zones = new HashMap<>();
        this.drones = new HashMap<>();
    }

    public static void main(String[] args) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        ObstacleAvoidanceSystem oas = new ObstacleAvoidanceSystem();
        System.out.println("STARTED MAIN");
        oas.startProcess();
    }

    public void startProcess() {

        FirebaseOptions options = null;

        FileInputStream serviceAccount = null;
        try {
            serviceAccount = new FileInputStream(""); // FIREBASE AUTHORIZATION JSON
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Could not read Auth key");
            return;
        }

        try {
            options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("") // FIREBASE DATABASE URL
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Auth key did not work");
            return;
        }

        FirebaseApp.initializeApp(options);

        final FirebaseDatabase DB = FirebaseDatabase.getInstance();

        DatabaseReference rootRef = DB.getReference();

        DatabaseReference droneRef = DB.getReference("Drone_data");
        DatabaseReference zoneRef = DB.getReference("Zone_data");


        //LISTENER FOR DRONE DB DATA UPDATE
        droneRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                if (snapshot != null) {
                    System.out.println("DRONE CHILD ADDED!");
                    DBDrone newDBDrone = (DBDrone) snapshot.getValue(DBDrone.class);
                    Drone newDrone = newDBDrone.toDrone();

                    synchronized (drones) {
                        drones.put(newDrone.getId(), newDrone);
                    }

                    System.out.println("ON UPDATE DRONE");


                    onUpdateDrone(newDrone.getId());
                    System.out.println("Zone length" + zones.size());
                    System.out.println("Drone length" + drones.size());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
                System.out.println("DRONE CHILD CHANGED!");
                DBDrone modifiedDBDrone = (DBDrone) snapshot.getValue(DBDrone.class);
                Drone modifiedDrone = modifiedDBDrone.toDrone();

                synchronized (drones) {
                    drones.put(modifiedDrone.getId(), modifiedDrone);
                }

                System.out.println("ON UPDATE DRONE");
                onUpdateDrone(modifiedDrone.getId());

                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }

            @Override
            public void onChildRemoved(DataSnapshot snapshot) {
                DBDrone removedDBDrone = (DBDrone) snapshot.getValue(DBDrone.class);
                Drone removedDrone = removedDBDrone.toDrone();

                synchronized (drones) {
                    drones.remove(removedDrone.getId());
                }

                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }

            @Override
            public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println("The read failed" + error.getCode());
                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }
        });

        //LISTENER FOR ZONE DB DATA UPDATE
        zoneRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                DBZone newDBZone = (DBZone) snapshot.getValue(DBZone.class);
                System.out.println("ZONE CHILD ADDED!");
                Zone newZone = newDBZone.toZone();

                synchronized (zones) {
                    zones.put(newZone.getId(), newZone);
                }

                System.out.println("ON UPDATE ZONE");
                onUpdateZone(newZone.getId());

                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }

            @Override
            public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
                DBZone modifiedDBZone = (DBZone) snapshot.getValue(DBZone.class);
                System.out.println("ZONE CHILD CHANGED!");
                Zone modifiedZone = modifiedDBZone.toZone();

                synchronized (zones) {
                    zones.put(modifiedZone.getId(), modifiedZone);
                }

                System.out.println("ON UPDATE ZONE");
                onUpdateZone(modifiedZone.getId());

                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }

            @Override
            public void onChildRemoved(DataSnapshot snapshot) {
                DBZone removeDBZone = (DBZone) snapshot.getValue(DBZone.class);
                Zone removedZone = removeDBZone.toZone();

                synchronized (zones) {
                    zones.remove(removedZone.getId());
                }

                System.out.println("Zone length" + zones.size());
                System.out.println("Drone length" + drones.size());
            }

            @Override
            public void onChildMoved(DataSnapshot snapshot, String previousChildName) {

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        while (true == true) {
        }

    }

    private void onUpdateDrone(String droneId) {
        isInsideProcess(new ArrayList<Drone>(Collections.singletonList(drones.get(droneId))), new ArrayList<>(zones.values()), false);
    }

    private void onUpdateZone(String zoneId) {
        isInsideProcess(new ArrayList<>(drones.values()), new ArrayList<Zone>(Collections.singletonList(zones.get(zoneId))), false);
    }

    private boolean quickZoneCheck(Drone drone, Zone zone) {

        double yDiff = drone.getLocation().getLatitude() - zone.getCenter().getLatitude();
        double xDiff = drone.getLocation().getLongitude() - zone.getCenter().getLongitude();

        double distance = (yDiff * yDiff) + (xDiff * xDiff);

        return distance < zone.getRadius() * zone.getRadius();

    }

    private void isInsideProcess(ArrayList<Drone> drones, ArrayList<Zone> zones, boolean IsInZone) {

        for (Drone drone : drones) {

            if (!isBeingControlled.containsKey(drone.getId()) || IsInZone) {

                for (Zone zone : zones) {

                    //QUICK RADIUS CHECK (VERY EFFICIENT, NOT 100% ACCURATE)
                    if (quickZoneCheck(drone, zone)) {

                        //LONG POSITION CHECK (LESS EFFICIENT, 100% ACCURATE)
                        if (!zone.getPolygonBased() || ComplexPointInPolygon.isInside(drone, zone)) {

                            if (!IsInZone) {
                                //CHECK ADD ZONE IN LIST OF DRONES IN ZONES (TO BE CONTROLLED)
                                isBeingControlled.put(drone.getId(), zone.getId());

                                new Thread(() -> {
                                    manageDroneInZone(drone.getId(), zone.getId());
                                }).start();

                            }


                        } else if (zone.getPolygonBased() && isBeingControlled.containsKey(drone.getId()) && isBeingControlled.get(drone.getId()).equals(zone.getId())) {
                            isBeingControlled.remove(drone.getId());
                            if(IsInZone){
                                isInsideProcess(new ArrayList<Drone>(Collections.singletonList(drone)), new ArrayList<Zone>(this.zones.values()), false);
                                return;
                            }

                        }
                    } else if (isBeingControlled.containsKey(drone.getId()) && isBeingControlled.get(drone.getId()).equals(zone.getId())) {
                        isBeingControlled.remove(drone.getId());
                        if(IsInZone){
                            isInsideProcess(new ArrayList<Drone>(Collections.singletonList(drone)), new ArrayList<Zone>(this.zones.values()), false);
                            return;
                        }
                    }
                }
            }
        }

    }

    private void manageDroneInZone(String droneId, String zoneId) {
        //Thread for management of Drone Automation

        int counter = 0;

        while (isBeingControlled.containsKey(droneId) && isBeingControlled.get(droneId).equals(zoneId)) {

            counter++;

            System.out.println("CROSSOVER WAS SENSED!");
            Message message = Message.creator(new PhoneNumber(droneTargetPhone), new PhoneNumber(TWILIO_NUMBER_MONTREAL),
                    "Iteration " + counter + "Drone " + droneId + " is inside of zone " + zoneId).create();

            isInsideProcess(new ArrayList<Drone>(Collections.singletonList(drones.get(droneId))), new ArrayList<Zone>(Collections.singletonList(zones.get(zoneId))), true);

            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
