package Models;

public class Drone {

    private String id;
    private int heading_angle;
    private Point location;

    public Drone(String id, int heading_angle, double latitude, double longitude) {
        this.id = id;
        this.heading_angle = heading_angle;
        this.location = new Point(latitude, longitude);
    }

    public String getId() {
        return id;
    }

    public int getHeading_angle() {
        return heading_angle;
    }

    public Point getLocation() {
        return location;
    }
}
