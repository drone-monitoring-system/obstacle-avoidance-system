package Models.DBModels;

import Models.Drone;

public class DBDrone {
    String heading_angle;
    String id;
    String latitude;
    String longitude;

    public DBDrone(){

    }

    public DBDrone(String heading_angle, String id, String latitude, String longitude) {
        this.heading_angle = heading_angle;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Drone toDrone(){
        return new Drone(id, Integer.parseInt(heading_angle), Double.parseDouble(latitude), Double.parseDouble(longitude));
    }

    public String getHeading_angle() {
        return heading_angle;
    }

    public void setHeading_angle(String heading_angle) {
        this.heading_angle = heading_angle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
