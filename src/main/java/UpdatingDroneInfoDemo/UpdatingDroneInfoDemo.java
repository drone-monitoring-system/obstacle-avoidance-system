package UpdatingDroneInfoDemo;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class UpdatingDroneInfoDemo {

    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = ""; // TWILIO ACCOUNT_SID
    public static final String AUTH_TOKEN = ""; // TWILIO AUTH_TOKEN

    public static final String TWILIO_NUMBER_FLORIDA = ""; // TWILIO NUMBER FROM FLORIDA WE HAD
    public static final String TWILIO_NUMBER_MONTREAL = ""; // TWILIO NUMBER FROM MONTREAL WE HAD

    public static void main(String[] args) {
        UpdatingDroneInfoDemo updatingDroneInfoDemo = new UpdatingDroneInfoDemo();
        updatingDroneInfoDemo.runDemo();

    }

    private void runDemo(){

        String[] droneA = {
                "D1_TANG,45,45.499527,-73.570334",
                "D1_TANG,46,45.499506,-73.570167",
                "D1_TANG,47,45.499480,-73.570035",
                "D1_TANG,48,45.499426,-73.569934",
                "D1_TANG,49,45.499320,-73.569984",
                "D1_TANG,50,45.499282,-73.570118",
                "D1_TANG,51,45.499313,-73.570252",
                "D1_TANG,52,45.499367,-73.570382",
                "D1_TANG,53,45.499421,-73.570484",
                "D1_TANG,54,45.499490,-73.570595",
                "D1_TANG,55,45.499531,-73.570552",
                "D1_TANG,56,45.499564,-73.570371"};

        String[] droneB = {
                "D2_COURNOYER,57,45.500291,-73.587619",
                "D2_COURNOYER,58,45.500163,-73.587668",
                "D2_COURNOYER,59,45.500113,-73.588064",
                "D2_COURNOYER,60,45.500158,-73.588477",
                "D2_COURNOYER,61,45.500229,-73.588863",
                "D2_COURNOYER,62,45.500297,-73.589120",
                "D2_COURNOYER,63,45.500515,-73.588922",
                "D2_COURNOYER,64,45.500429,-73.588648",
                "D2_COURNOYER,65,45.500425,-73.588428",
                "D2_COURNOYER,66,45.500455,-73.588187",
                "D2_COURNOYER,67,45.500429,-73.587919",
                "D2_COURNOYER,68,45.500354,-73.587731"};

        String[] droneC = {
                "D4_HUN,69,45.581039,-73.347870",
                "D4_HUN,70,45.581065,-73.348390",
                "D4_HUN,71,45.580705,-73.348690",
                "D4_HUN,72,45.580352,-73.348931",
                "D4_HUN,73,45.579909,-73.348738",
                "D4_HUN,74,45.579830,-73.348293",
                "D4_HUN,75,45.579804,-73.347746",
                "D4_HUN,76,45.580089,-73.347483",
                "D4_HUN,77,45.580585,-73.347344",
                "D4_HUN,78,45.581058,-73.347419",
                "D4_HUN,79,45.581490,-73.347714",
                "D4_HUN,80,45.581201,-73.348090"};

        Thread droneAThread = new Thread(new SendSMS("Drone1", droneA));
        Thread droneBThread = new Thread(new SendSMS("Drone2", droneB));
        Thread droneCThread = new Thread(new SendSMS("Drone3", droneC));

        droneAThread.start();
//        droneBThread.start();
//        droneCThread.start();

    }

    private class SendSMS implements Runnable {

        private String FBIdentifier;
        private String[] message;

        SendSMS(String FBIdentifier, String[] message) {
            this.FBIdentifier = FBIdentifier;
            this.message = message;
        }

        @Override
        public void run() {

            FirebaseOptions options = null;

            FileInputStream serviceAccount =
                    null;
            try {
                serviceAccount = new FileInputStream(""); //DIR TO FIREBASE AUTHORIZATION JSON KEY
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("Could not read Auth key");
                return;
            }

            try {
                options = new FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                        .setDatabaseUrl("") //LINK TO FIREBASE DATABASE
                        .build();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Auth key did not work");
                return;
            }

            FirebaseApp.initializeApp(options);

            final FirebaseDatabase DB = FirebaseDatabase.getInstance();

            DatabaseReference rootRef = DB.getReference();

            DatabaseReference childRef = rootRef.child("Drone_data");

            int timer = 0;

            while (timer < 5000) {
                int i = 0;

                for (i = 0; i < message.length; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    String[] subMessages = message[i].split(",");

                    HashMap<String, Object> droneUpdates = new HashMap<>();
                    droneUpdates.put(FBIdentifier + "/id", subMessages[0]);
                    droneUpdates.put(FBIdentifier + "/heading_angle", subMessages[1]);
                    droneUpdates.put(FBIdentifier + "/latitude", subMessages[2]);
                    droneUpdates.put(FBIdentifier + "/longitude", subMessages[3]);

                    System.out.println(FBIdentifier + " : " + Arrays.toString(subMessages));

                    childRef.updateChildrenAsync(droneUpdates);
                }

                timer++;

            }
        }
    }

    //How to start receiving messages:
    //Have twilio installed in cli, have ngrok installed

    //Write in CLI : twilio phone-numbers:update "<TWILIO NUMBER HERE>" --sms-url="http://localhost:4567/sms"

}
