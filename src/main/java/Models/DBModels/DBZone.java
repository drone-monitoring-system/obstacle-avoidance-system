package Models.DBModels;

import Models.Point;
import Models.Zone;

import java.util.ArrayList;
import java.util.List;

public class DBZone {

    String id;
    String latitudes;
    String longitudes;
    String center;
    double radius;
    Boolean polygonBased;

    public DBZone(){

    }

    public DBZone(String id, String latitudes, String longitudes, String center, double radius, Boolean polygonBased) {
        this.id = id;
        this.latitudes = latitudes;
        this.longitudes = longitudes;
        this.center = center;
        this.radius = radius;
        this.polygonBased = polygonBased;
    }

    public Zone toZone(){

        String[] latitudes = this.latitudes.split(",");
        String[] longitudes = this.longitudes.split(",");

        List<Point> edges = new ArrayList<>();

        for(int i = 0 ; i < latitudes.length; i++){
            edges.add(new Point(Double.parseDouble(latitudes[i]), Double.parseDouble(longitudes[i])));
        }

        String[] centerCoords = center.split(",");

        Point center = new Point(Double.parseDouble(centerCoords[0]), Double.parseDouble(centerCoords[1]));

        return new Zone(id, edges, center,radius, polygonBased);

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitudes() {
        return latitudes;
    }

    public void setLatitudes(String latitudes) {
        this.latitudes = latitudes;
    }

    public String getLongitudes() {
        return longitudes;
    }

    public void setLongitudes(String longitudes) {
        this.longitudes = longitudes;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Boolean getPolygonBased() {
        return polygonBased;
    }

    public void setPolygonBased(Boolean polygonBased) {
        this.polygonBased = polygonBased;
    }
}
